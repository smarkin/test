/*
This file is used to bootstrap development database.

Note: ONLY development database;
*/

CREATE USER test SUPERUSER;
CREATE DATABASE test OWNER test ENCODING 'utf-8';
