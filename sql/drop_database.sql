/*
This file is used to drop development database;

Note: ONLY development database;
*/

DROP DATABASE test;
DROP USER test;
